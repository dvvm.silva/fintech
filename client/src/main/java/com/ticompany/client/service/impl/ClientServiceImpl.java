package com.ticompany.client.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.ticompany.client.dto.ClientDto;
import com.ticompany.client.entity.ClientEntity;
import com.ticompany.client.exeption.BusinessExeption;
import com.ticompany.client.repository.ClientRepository;
import com.ticompany.client.service.ClientService;
import com.ticompany.client.util.Util;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@Log4j2
@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ModelMapper mapper;

    @Override
    public ClientDto save(ClientDto clientDto) {
        checksClient(clientDto);
        ClientEntity clientEntity = this.clientRepository.save(this.mapper.map(clientDto, ClientEntity.class));
        return this.mapper.map(clientEntity, ClientDto.class);
    }

    @Override
    public ClientDto getByCpf(String cpf) {
        ClientEntity clientEntity = this.clientRepository.findByCpf(cpf);
        log.info("Looking for customer by cpf {}", cpf);
        checkIfCustomerExists(clientEntity);
        return this.mapper.map(clientEntity, ClientDto.class);
    }

    private void checkIfCustomerExists(ClientEntity clientEntity) {
        log.info("checking if customer exists {}", clientEntity);
        if (Util.isNull(clientEntity)) {
            throw new BusinessExeption("Cliente não encontrado!");
        }
    }

    private void checksClient(ClientDto clientDto) {
        log.info("checking ClientDto {}", clientDto);

        if (Util.isNull(clientDto.getCpf()))
            throw new BusinessExeption("Campo cpf é obrigatorio.");

        if (Util.isNull(clientDto.getName()))
            throw new BusinessExeption("Campo nome é obrigatorio.");

        if (Util.isNotNull(this.clientRepository.findByCpf(clientDto.getCpf())))
            throw new BusinessExeption("Cliente já cadastrado!");
    }

}
