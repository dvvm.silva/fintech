package com.ticompany.client.service;

import com.ticompany.client.dto.ClientDto;

public interface ClientService {

    public ClientDto save(ClientDto clientDto);

    public ClientDto getByCpf(String cpf);
}
