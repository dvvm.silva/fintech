package com.ticompany.client.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientDto {

    private Long id;
    private String cpf;
    private String name;
    private Integer yearsOld;
}