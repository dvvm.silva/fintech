package com.ticompany.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ticompany.client.entity.ClientEntity;

public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    ClientEntity findByCpf(String cpf);
}