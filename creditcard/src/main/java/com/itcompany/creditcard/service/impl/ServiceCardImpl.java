package com.itcompany.creditcard.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.itcompany.creditcard.domain.Card;
import com.itcompany.creditcard.dto.CardDto;
import com.itcompany.creditcard.exeption.BusinessExeption;
import com.itcompany.creditcard.repository.RepositoryCard;
import com.itcompany.creditcard.service.ServiceCard;
import com.itcompany.creditcard.util.Util;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ServiceCardImpl implements ServiceCard {

    private final RepositoryCard repositoryCard;
    private final ModelMapper modelMapper;

    @Override
    public CardDto save(CardDto cardDto) {
        checkCard(cardDto);
        return this.modelMapper.map(
                this.repositoryCard.save(
                        this.modelMapper.map(
                                cardDto, Card.class)),
                CardDto.class);
    }

    @Override
    public List<CardDto> getCardByIncome(@RequestParam Long income) {
        List<Card> cardList = this.repositoryCard.findByIncomeLessThanEqual(BigDecimal.valueOf(income));
        return cardList.stream().map(CardDto::new).collect(Collectors.toList());
    }

    @Override
    public CardDto getById(Long id) {
        if (this.repositoryCard.findById(id).isPresent()) {
            return this.modelMapper.map(this.repositoryCard.findById(id).get(), CardDto.class);
        } else {
            throw new BusinessExeption("Cartão não castrado!");
        }
    }

    private void checkCard(CardDto cardDto) {

        if (Util.isNull(cardDto.getBasicLimit()))
            throw new BusinessExeption("Limeti do cartão não informado.");

        if (Util.isNull(cardDto.getCardFlag()))
            throw new BusinessExeption("Bandeira do cartão não informado.");

        if (Util.isNull(cardDto.getName()))
            throw new BusinessExeption("Nome do cartão não informado.");

        if (Util.isNull(cardDto.getIncome()))
            throw new BusinessExeption("Renda minima não informada.");

        this.repositoryCard.findByNameAndBasicLimit(cardDto.getName(), cardDto.getBasicLimit())
                .ifPresent(card -> {
                    throw new BusinessExeption("Cartão já castrado!");
                });
    }

}
