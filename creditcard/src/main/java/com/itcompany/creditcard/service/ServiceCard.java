package com.itcompany.creditcard.service;

import java.util.List;

import com.itcompany.creditcard.dto.CardDto;

public interface ServiceCard {

    public CardDto save(CardDto cardDto);

    public List<CardDto> getCardByIncome(Long income);

    public CardDto getById(Long id);
}
