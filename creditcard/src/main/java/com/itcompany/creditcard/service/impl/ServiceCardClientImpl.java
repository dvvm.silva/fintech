package com.itcompany.creditcard.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.itcompany.creditcard.domain.Card;
import com.itcompany.creditcard.domain.CardClient;
import com.itcompany.creditcard.domain.model.CardRequest;
import com.itcompany.creditcard.dto.CardClientDto;
import com.itcompany.creditcard.repository.RepositoryCard;
import com.itcompany.creditcard.repository.RepositoryCardClient;
import com.itcompany.creditcard.service.ServiceCardClient;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ServiceCardClientImpl implements ServiceCardClient {

    private final RepositoryCardClient repositoryCardClient;
    private final RepositoryCard repositoryCard;

    @Override
    public List<CardClientDto> getCardByClient(String cpf) {
        var cardClientList = this.repositoryCardClient.findByCpf(cpf);

        return cardClientList.stream().map(CardClientDto::new).collect(Collectors.toList());
    }

    @Override
    public void requestCardForClient(CardRequest cardRequest) {
        var card = this.repositoryCard.findById(cardRequest.getCardId());

        var cardClient = CardClient.builder()
                .cpf(cardRequest.getCpf())
                .card(card.get())
                .limite(cardRequest.getLimitReleased())
                .build();

        this.repositoryCardClient.save(cardClient);
    }
}
