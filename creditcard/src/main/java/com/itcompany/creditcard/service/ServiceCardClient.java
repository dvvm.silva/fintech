package com.itcompany.creditcard.service;

import java.util.List;

import com.itcompany.creditcard.domain.model.CardRequest;
import com.itcompany.creditcard.dto.CardClientDto;

public interface ServiceCardClient {

    public List<CardClientDto> getCardByClient(String cpf);

    public void requestCardForClient(CardRequest cardRequest);
}
