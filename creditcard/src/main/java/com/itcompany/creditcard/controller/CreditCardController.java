package com.itcompany.creditcard.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itcompany.creditcard.dto.CardClientDto;
import com.itcompany.creditcard.dto.CardDto;
import com.itcompany.creditcard.service.ServiceCard;
import com.itcompany.creditcard.service.ServiceCardClient;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api-credit")
@RequiredArgsConstructor
public class CreditCardController {

    private final ServiceCard serviceCard;
    private final ServiceCardClient serviceCardClient;

    @PostMapping
    public ResponseEntity<CardDto> save(@RequestBody CardDto cardDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.serviceCard.save(cardDto));
    }

    @GetMapping(params = "income")
    public ResponseEntity<List<CardDto>> getCardByIncome(@RequestParam(required = true) Long income) {

        return ResponseEntity.ok(this.serviceCard.getCardByIncome(income));
    }

    @GetMapping(params = "cpf")
    public ResponseEntity<List<CardClientDto>> getCardByIncome(@RequestParam String cpf) {
        return ResponseEntity.ok(this.serviceCardClient.getCardByClient(cpf));
    }

    @GetMapping(params = "id")
    public ResponseEntity<CardDto> getCard(@RequestParam Long id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.serviceCard.getById(id));
    }
}
