package com.itcompany.creditcard.exeption.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.itcompany.creditcard.exeption.BusinessExeption;
import com.itcompany.creditcard.util.ErrorResponse;

import lombok.extern.log4j.Log4j2;

@Log4j2
@ControllerAdvice
@RestController
public class GlobalExeceptionHandler {

    @ExceptionHandler(value = BusinessExeption.class)
    public ResponseEntity<ErrorResponse> handlerBussinessExeception(BusinessExeption e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponse> handlerBussinessExeception(MissingServletRequestParameterException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handlerExeception(Exception e) {
        log.error("{}", e);
        return new ResponseEntity<>(
                new ErrorResponse(
                        "Erro tente novamente, caso o erro persista entre em contato com o administrador do sistema."),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
