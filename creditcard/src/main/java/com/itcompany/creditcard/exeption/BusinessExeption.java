package com.itcompany.creditcard.exeption;

public class BusinessExeption extends RuntimeException {

    public BusinessExeption() {
        super();
    }

    public BusinessExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BusinessExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessExeption(String message) {
        super(message);
    }

    public BusinessExeption(Throwable cause) {
        super(cause);
    }

}
