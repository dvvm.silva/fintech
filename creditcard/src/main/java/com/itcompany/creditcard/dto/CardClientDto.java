package com.itcompany.creditcard.dto;

import com.itcompany.creditcard.domain.Card;
import com.itcompany.creditcard.domain.CardClient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CardClientDto {

    private Long id;
    private String cpf;
    private Card card;

    public CardClientDto(CardClient cardClient) {
        this.id = cardClient.getId();
        this.cpf = cardClient.getCpf();
        this.card = cardClient.getCard();
    }

}
