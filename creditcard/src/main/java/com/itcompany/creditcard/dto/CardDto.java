package com.itcompany.creditcard.dto;

import java.math.BigDecimal;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.itcompany.creditcard.domain.Card;
import com.itcompany.creditcard.enumeration.CardFlag;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardDto {

    private Long id;
    private String name;
    @Enumerated(EnumType.STRING)
    private CardFlag cardFlag;
    private BigDecimal income;
    private BigDecimal basicLimit;

    public CardDto(Card card) {

        this.id = card.getId();
        this.name = card.getName();
        this.cardFlag = card.getCardFlag();
        this.income = card.getIncome();
        this.basicLimit = card.getBasicLimit();
    }

    public CardDto() {
    }

}
