package com.itcompany.creditcard.listeners;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.itcompany.creditcard.domain.model.CardRequest;
import com.itcompany.creditcard.service.ServiceCardClient;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
@Component
public class ListenerRequestCard {

    private final ServiceCardClient serviceCardClient;

    @KafkaListener(topics = "card-request", groupId = "request-card-group", containerFactory = "containerFactoryCardRequest")
    public void requestCard(@Payload CardRequest cardRequest) {
        log.info("Gerando cartao: {}", cardRequest);
        this.serviceCardClient.requestCardForClient(cardRequest);
    }
}
