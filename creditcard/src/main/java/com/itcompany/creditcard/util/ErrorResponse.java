package com.itcompany.creditcard.util;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class ErrorResponse {

    @JsonProperty
    private final String message;

}
