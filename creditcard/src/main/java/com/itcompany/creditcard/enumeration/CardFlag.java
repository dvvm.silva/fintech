package com.itcompany.creditcard.enumeration;

public enum CardFlag {

    MASTERCARD,
    VISA
}
