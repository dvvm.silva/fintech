package com.itcompany.creditcard.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itcompany.creditcard.domain.Card;

public interface RepositoryCard extends JpaRepository<Card, Long> {

    List<Card> findByIncomeLessThanEqual(BigDecimal income);

    Optional<Card> findByNameAndBasicLimit(String name, BigDecimal basicLimit);
}
