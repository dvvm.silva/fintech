package com.itcompany.creditcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itcompany.creditcard.domain.CardClient;

public interface RepositoryCardClient extends JpaRepository<CardClient, Long> {

    List<CardClient> findByCpf(String cpf);

}
