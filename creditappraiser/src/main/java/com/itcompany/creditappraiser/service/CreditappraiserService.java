package com.itcompany.creditappraiser.service;

import com.itcompany.creditappraiser.domain.model.CardRequest;
import com.itcompany.creditappraiser.domain.model.CustomerSituation;
import com.itcompany.creditappraiser.domain.model.EvaluationResponse;

public interface CreditappraiserService {
    public CustomerSituation getCustomerSituation(String cpf);

    public EvaluationResponse performEvaluation(String cpf, Long income);

    public void cardCreditRequest(CardRequest cardRequest);
}
