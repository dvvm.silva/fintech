package com.itcompany.creditappraiser.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.itcompany.creditappraiser.clients.CardCredit;
import com.itcompany.creditappraiser.clients.ClientResource;
import com.itcompany.creditappraiser.domain.model.ApprovedCard;
import com.itcompany.creditappraiser.domain.model.Card;
import com.itcompany.creditappraiser.domain.model.CardRequest;
import com.itcompany.creditappraiser.domain.model.CustomerCard;
import com.itcompany.creditappraiser.domain.model.CustomerData;
import com.itcompany.creditappraiser.domain.model.CustomerSituation;
import com.itcompany.creditappraiser.domain.model.EvaluationResponse;
import com.itcompany.creditappraiser.exeption.BusinessExeption;
import com.itcompany.creditappraiser.service.CreditappraiserService;
import com.itcompany.creditappraiser.util.Util;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CreditappraiserServiceImpl implements CreditappraiserService {

    private final ClientResource clientResource;
    private final CardCredit cardResource;
    private final CardCredit cardCredit;
    private final KafkaTemplate<String, CardRequest> kafkaTemplate;

    @Override
    public CustomerSituation getCustomerSituation(String cpf) {

        ResponseEntity<CustomerData> customerDataResponse = this.clientResource.getByCpf(cpf);
        ResponseEntity<List<CustomerCard>> customerCardResponse = this.cardResource.getCardByIncome(cpf);
        return CustomerSituation.builder()
                .customerData(customerDataResponse.getBody())
                .customerCardList(customerCardResponse.getBody())
                .build();
    }

    @Override
    public EvaluationResponse performEvaluation(String cpf, Long income) {
        ResponseEntity<CustomerData> customerDataResponse = this.clientResource.getByCpf(cpf);
        ResponseEntity<List<Card>> cardResponse = this.cardCredit.getCardByIncome(income);
        List<Card> cardList = cardResponse.getBody();

        checkResponse(cardList);

        var cardApprovedList = cardList.stream().map(card -> {
            var basicLimit = card.getBasicLimit();
            var yearsOld = BigDecimal.valueOf(customerDataResponse.getBody().getYearsOld());
            var factor = yearsOld.divide(BigDecimal.valueOf(10));
            var approvedLimit = factor.multiply(basicLimit);
            return ApprovedCard.builder()
                    .name(card.getName())
                    .flag(card.getCardFlag())
                    .approvedLimit(approvedLimit)
                    .build();
        }).collect(Collectors.toList());

        return new EvaluationResponse(cardApprovedList);
    }

    private void checkResponse(List<Card> cardList) {
        if (Util.isNull(cardList))
            throw new BusinessExeption("Não há cartão disponivel para renda informada.");
    }

    @Override
    public void cardCreditRequest(CardRequest cardRequest) {

        this.kafkaTemplate.send("card-request", cardRequest);
    }

}
