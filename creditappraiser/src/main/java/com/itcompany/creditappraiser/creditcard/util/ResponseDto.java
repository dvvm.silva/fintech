package com.itcompany.creditappraiser.creditcard.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(value = Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {

    private Long id;
    private String message;
    private T value;
    private List<String> comments;
}
