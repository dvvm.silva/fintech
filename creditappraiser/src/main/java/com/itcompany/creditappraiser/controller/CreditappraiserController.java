package com.itcompany.creditappraiser.controller;

import java.io.Serializable;

import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itcompany.creditappraiser.domain.model.CardRequest;
import com.itcompany.creditappraiser.domain.model.CustomerSituation;
import com.itcompany.creditappraiser.domain.model.EvaluationData;
import com.itcompany.creditappraiser.service.CreditappraiserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api-credit-appraiser")
@RequiredArgsConstructor
public class CreditappraiserController {

    private final CreditappraiserService creditappraiserService;

    @GetMapping
    public ResponseEntity<CustomerSituation> consultCustomerStatus(@RequestParam String cpf) {
        return ResponseEntity.ok().body(this.creditappraiserService.getCustomerSituation(cpf));
    }

    @PostMapping
    public ResponseEntity<?> performEvaluation(@RequestBody EvaluationData evaluationData) {

        return ResponseEntity.ok().body(
                this.creditappraiserService.performEvaluation(evaluationData.getCpf(), evaluationData.getRenda()));
    }

    @PostMapping("/requirement-card")
    public ResponseEntity<?> cardRequest(@RequestBody CardRequest cardRequest) {
        this.creditappraiserService.cardCreditRequest(cardRequest);
        return ResponseEntity.ok().build();
    }
}
