package com.itcompany.creditappraiser.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.itcompany.creditappraiser.domain.model.CustomerData;

@FeignClient(value = "msclient", path = "/api-client")
public interface ClientResource {

    @GetMapping
    public ResponseEntity<CustomerData> getByCpf(@RequestParam String cpf);
}
