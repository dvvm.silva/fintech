package com.itcompany.creditappraiser.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.itcompany.creditappraiser.domain.model.Card;
import com.itcompany.creditappraiser.domain.model.CustomerCard;

@FeignClient(value = "creditcard", path = "/api-credit")
public interface CardCredit {

    @GetMapping(params = "income")
    public ResponseEntity<List<Card>> getCardByIncome(@RequestParam(required = true) Long income);

    @GetMapping(params = "cpf")
    public ResponseEntity<List<CustomerCard>> getCardByIncome(@RequestParam String cpf);

    @GetMapping(params = "id")
    public ResponseEntity<Card> getCard(@RequestParam Long id);
}
