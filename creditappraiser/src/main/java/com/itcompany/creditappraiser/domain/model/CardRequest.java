package com.itcompany.creditappraiser.domain.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CardRequest {

    private Long cardId;
    private String cpf;
    private String address;
    private BigDecimal limitReleased;

}
