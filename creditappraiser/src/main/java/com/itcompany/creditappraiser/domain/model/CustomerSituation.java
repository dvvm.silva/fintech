package com.itcompany.creditappraiser.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerSituation {

    private CustomerData customerData;
    private List<CustomerCard> customerCardList;
}
