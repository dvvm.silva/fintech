package com.itcompany.creditappraiser.domain.model;

import lombok.Data;

@Data
public class EvaluationData {

    private String cpf;
    private Long renda;
}
