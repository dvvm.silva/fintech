package com.itcompany.creditappraiser.domain.model;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApprovedCard {

    private String name;
    private String flag;
    private BigDecimal approvedLimit;

}
