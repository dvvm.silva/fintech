package com.itcompany.creditappraiser.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EvaluationResponse {
    private List<ApprovedCard> approvedCards;
}
