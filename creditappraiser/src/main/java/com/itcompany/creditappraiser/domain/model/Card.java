package com.itcompany.creditappraiser.domain.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Card {
    private Long id;
    private String name;
    private String cardFlag;
    private BigDecimal basicLimit;
}
