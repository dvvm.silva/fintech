package com.itcompany.creditappraiser.util;

import lombok.Data;

@Data
public class Util {

    public static boolean isNull(String value) {
        return value == null || (value.trim().length() == 0);
    }

    public static boolean isNull(Object... values) {
        for (Object value : values) {
            if (isNull(value)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isNotNull(Object... values) {
        return !isNull(values);
    }

    public static boolean isNull(Object value) {
        return value instanceof String ? isNull((String) value) : value == null;
    }
}
