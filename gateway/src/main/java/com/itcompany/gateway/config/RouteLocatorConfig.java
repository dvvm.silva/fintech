package com.itcompany.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteLocatorConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder
                .routes()
                .route(rota -> rota.path("client/**").uri("lb://msclient"))
                .route(rota -> rota.path("api-credit/**").uri("lb://creditcard"))
                .route(rota -> rota.path("api-credit-appraiser/**").uri("lb://creditappraiser"))
                .build();
    }
}
